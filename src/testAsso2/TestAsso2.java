package testAsso2;

/*Objets d�association*/

//Importer les classes jdbc
import java.sql.*;
import java.util.*;

public class TestAsso2 {
	
	static ArrayList<Etudiant> listeEtudiant = new ArrayList<Etudiant>();
	static ArrayList<Module> listeModule = new ArrayList<Module>();
	
	// La requete de test
	static final String req = "SELECT MOY_CC, MOY_TEST " + "FROM NOTATION " + "WHERE NUM_ET = ? " + "AND CODE = ?";
	
	public static void main(String[] args) throws SQLException {
		
		// Objet materialisant la connexion a la base de donnees
		System.out.println("Connexion a ");
		
		try (Connection conn = ConnectionUnique.getInstance().getConnection()) {
			
			// Connexion a la base
			System.out.println("Connecte\n");
			
			//Creation d'une pre-instruction
			PreparedStatement prepared = conn.prepareStatement(req);
			prepared.setString(2, "ACSI");
			
			// Execution de la requete
			System.out.println("Execution de la requete : " + req + "\n");
			ResultSet rset = prepared.executeQuery(req);
			
			// Affichage du resultat
			while (rset.next()) {
				Etudiant E = new Etudiant();
				Module M = new Module();
				M.setCode(rset.getString("CODE"));
				E.setNumEt(M.getEtudiants().getEtudiant().getNumEt());
				prepared.setInt(1,E.getNumEt());
			}
			// Fermeture de l'instruction (liberation des ressources)
			prepared.close();
			//System.out.println(.toString() ); //afichage du tableau de prof, l'affichage de prof a chaque it�rarion est plus lisible.
			System.out.println("\nOk.\n");
		} catch (SQLException e) {
			e.printStackTrace();// Arggg!!!
			System.out.println(e.getMessage() + "\n");
		} 
	}
}
