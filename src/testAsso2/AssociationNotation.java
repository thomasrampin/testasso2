package testAsso2;

import java.util.Iterator;
import java.util.TreeSet;
import java.util.Set;

public class AssociationNotation {
	
	private Set<Lien> liens = new TreeSet<Lien>();
	private static AssociationNotation instance;
	
	public Set<Lien> getLiens(Etudiant E) {
		return liens;
	}
	
	public Set<Lien> getLiens(Module M) {
		return liens;
	}
	
	public static AssociationNotation getInstance() {
		if(instance==null)
			instance = new AssociationNotation();
		return instance;
	}
	
	public void creerLien(Module M, Etudiant E, Notation N){
		Lien newLink = new Lien(M,E,N);
		this.liens.add(newLink);
	}
	
	public void suprimerLien(Module M, Etudiant E){
		liens.remove(getLien(M,E));
	}
	
	public void suprimerLien(Lien L) {
		liens.remove(L);
	}
	
	public Lien getLien(Module M, Etudiant E){
		Lien compareLink = new Lien(M,E,null);
		Iterator<Lien> i=liens.iterator();
		
		while(i.hasNext()){
			i.equals(compareLink);
		}
		return compareLink;
	}
	
	public Set<Module> getModules(Etudiant E){
		Iterator<Lien> i=liens.iterator();
		Set<Module> modules = new TreeSet<Module>();
		while(i.hasNext()){
			Lien item = (Lien) i.next();
			if(item.equals(new Lien (item.getModule(), E, null))){
				
				modules.add(item.getModule());
			}
		}
		return modules;
	}
	
	public Set<Etudiant> getEtudiants(Module M){
		Iterator<Lien> i=liens.iterator();
		Set<Etudiant> etudiants = new TreeSet<Etudiant>();
		while(i.hasNext()){
			Lien item = (Lien) i.next();
			if(item.equals(new Lien (M, item.getEtudiant(), null))){
				
				etudiants.add(item.getEtudiant());
			}
		}
		return etudiants;
	}
	
	//Constructeur priv�, marque de fabrique d'un singleton
	private AssociationNotation() {
		
	}
	
}
