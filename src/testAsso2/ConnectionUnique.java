package testAsso2;

import java.sql.*;

public final class ConnectionUnique {
	
	private Connection connexion;
	
	private static ConnectionUnique instance;
	
	static final String CONNECT_URL = "jdbc:mysql://localhost:3306/mysqlposey";
	static final String LOGIN = "root";
	static final String PASSWORD = "root";
	
	private ConnectionUnique(){
		try{
			connexion = DriverManager.getConnection(CONNECT_URL, LOGIN, PASSWORD);
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	};
	
	public static ConnectionUnique getInstance(){ 
		if(instance==null)
			instance = new ConnectionUnique();
		return instance;};
		
	public Connection getConnection(){
		return connexion;
	};
	
}
